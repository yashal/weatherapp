package com.example.weatherandroidassignment.data.db;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007J\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0007J\u0014\u0010\t\u001a\u0004\u0018\u00010\u00042\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0007J\u0014\u0010\f\u001a\u0004\u0018\u00010\u00042\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0007J\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004H\u0007J\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u000b2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004H\u0007J\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u000e2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004H\u0007J\u0014\u0010\u0013\u001a\u0004\u0018\u00010\b2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004H\u0007J&\u0010\u0014\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u00010\u00160\u0015j\n\u0012\u0006\u0012\u0004\u0018\u00010\u0016`\u00172\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004H\u0007J\u0014\u0010\u0018\u001a\u0004\u0018\u00010\u00192\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004H\u0007J*\u0010\u001a\u001a\u0004\u0018\u00010\u00042\u001e\u0010\u001b\u001a\u001a\u0012\u0006\u0012\u0004\u0018\u00010\u0016\u0018\u00010\u0015j\f\u0012\u0006\u0012\u0004\u0018\u00010\u0016\u0018\u0001`\u0017H\u0007J\u0014\u0010\u001c\u001a\u0004\u0018\u00010\u00042\b\u0010\u001d\u001a\u0004\u0018\u00010\u0019H\u0007\u00a8\u0006\u001e"}, d2 = {"Lcom/example/weatherandroidassignment/data/db/Converters;", "", "()V", "cloudsDataToString", "", "cloudsData", "Lcom/example/weatherandroidassignment/data/network/response/CloudsData;", "sunsetData", "Lcom/example/weatherandroidassignment/data/network/response/SunsetData;", "coordinateDataToString", "coordinateData", "Lcom/example/weatherandroidassignment/data/network/response/CoordinateData;", "mainDataToString", "mainData", "Lcom/example/weatherandroidassignment/data/network/response/MainData;", "stringToCloudsData", "data", "stringToCoordinateData", "stringToMainData", "stringToSunsetData", "stringToWeatherDataList", "Ljava/util/ArrayList;", "Lcom/example/weatherandroidassignment/data/network/response/WeatherData;", "Lkotlin/collections/ArrayList;", "stringToWindData", "Lcom/example/weatherandroidassignment/data/network/response/WindData;", "weatherDataListToString", "someObjects", "windDataToString", "windData", "app_debug"})
public final class Converters {
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.TypeConverter()
    public final java.util.ArrayList<com.example.weatherandroidassignment.data.network.response.WeatherData> stringToWeatherDataList(@org.jetbrains.annotations.Nullable()
    java.lang.String data) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.lang.String weatherDataListToString(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<com.example.weatherandroidassignment.data.network.response.WeatherData> someObjects) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final com.example.weatherandroidassignment.data.network.response.CoordinateData stringToCoordinateData(@org.jetbrains.annotations.Nullable()
    java.lang.String data) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.lang.String coordinateDataToString(@org.jetbrains.annotations.Nullable()
    com.example.weatherandroidassignment.data.network.response.CoordinateData coordinateData) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final com.example.weatherandroidassignment.data.network.response.MainData stringToMainData(@org.jetbrains.annotations.Nullable()
    java.lang.String data) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.lang.String mainDataToString(@org.jetbrains.annotations.Nullable()
    com.example.weatherandroidassignment.data.network.response.MainData mainData) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final com.example.weatherandroidassignment.data.network.response.WindData stringToWindData(@org.jetbrains.annotations.Nullable()
    java.lang.String data) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.lang.String windDataToString(@org.jetbrains.annotations.Nullable()
    com.example.weatherandroidassignment.data.network.response.WindData windData) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final com.example.weatherandroidassignment.data.network.response.CloudsData stringToCloudsData(@org.jetbrains.annotations.Nullable()
    java.lang.String data) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.lang.String cloudsDataToString(@org.jetbrains.annotations.Nullable()
    com.example.weatherandroidassignment.data.network.response.CloudsData cloudsData) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final com.example.weatherandroidassignment.data.network.response.SunsetData stringToSunsetData(@org.jetbrains.annotations.Nullable()
    java.lang.String data) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.lang.String cloudsDataToString(@org.jetbrains.annotations.Nullable()
    com.example.weatherandroidassignment.data.network.response.SunsetData sunsetData) {
        return null;
    }
    
    public Converters() {
        super();
    }
}