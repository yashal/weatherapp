package com.example.weatherandroidassignment.data.network.response;

import java.lang.System;

@androidx.room.Entity()
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b$\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0087\b\u0018\u00002\u00020\u0001Bu\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0016\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\f0\u000bj\b\u0012\u0004\u0012\u00020\f`\r\u0012\u0006\u0010\u000e\u001a\u00020\u0005\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u00a2\u0006\u0002\u0010\u0019J\t\u0010/\u001a\u00020\u0003H\u00c6\u0003J\t\u00100\u001a\u00020\u0014H\u00c6\u0003J\t\u00101\u001a\u00020\u0016H\u00c6\u0003J\t\u00102\u001a\u00020\u0018H\u00c6\u0003J\t\u00103\u001a\u00020\u0005H\u00c6\u0003J\t\u00104\u001a\u00020\u0003H\u00c6\u0003J\t\u00105\u001a\u00020\u0003H\u00c6\u0003J\t\u00106\u001a\u00020\tH\u00c6\u0003J\u0019\u00107\u001a\u0012\u0012\u0004\u0012\u00020\f0\u000bj\b\u0012\u0004\u0012\u00020\f`\rH\u00c6\u0003J\t\u00108\u001a\u00020\u0005H\u00c6\u0003J\t\u00109\u001a\u00020\u0010H\u00c6\u0003J\t\u0010:\u001a\u00020\u0012H\u00c6\u0003J\u0091\u0001\u0010;\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\t2\u0018\b\u0002\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\f0\u000bj\b\u0012\u0004\u0012\u00020\f`\r2\b\b\u0002\u0010\u000e\u001a\u00020\u00052\b\b\u0002\u0010\u000f\u001a\u00020\u00102\b\b\u0002\u0010\u0011\u001a\u00020\u00122\b\b\u0002\u0010\u0013\u001a\u00020\u00142\b\b\u0002\u0010\u0015\u001a\u00020\u00162\b\b\u0002\u0010\u0017\u001a\u00020\u0018H\u00c6\u0001J\u0013\u0010<\u001a\u00020=2\b\u0010>\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010?\u001a\u00020\u0003H\u00d6\u0001J\t\u0010@\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u000e\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0011\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0011\u0010\b\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0011\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010\u001fR\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010&R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\'\u0010\u001bR\u0011\u0010\u0017\u001a\u00020\u0018\u00a2\u0006\b\n\u0000\u001a\u0004\b(\u0010)R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b*\u0010\u001fR!\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\f0\u000bj\b\u0012\u0004\u0012\u00020\f`\r\u00a2\u0006\b\n\u0000\u001a\u0004\b+\u0010,R\u0011\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\b\n\u0000\u001a\u0004\b-\u0010.\u00a8\u0006A"}, d2 = {"Lcom/example/weatherandroidassignment/data/network/response/WeatherResponse;", "", "id", "", "name", "", "cod", "timezone", "coord", "Lcom/example/weatherandroidassignment/data/network/response/CoordinateData;", "weather", "Ljava/util/ArrayList;", "Lcom/example/weatherandroidassignment/data/network/response/WeatherData;", "Lkotlin/collections/ArrayList;", "base", "main", "Lcom/example/weatherandroidassignment/data/network/response/MainData;", "wind", "Lcom/example/weatherandroidassignment/data/network/response/WindData;", "clouds", "Lcom/example/weatherandroidassignment/data/network/response/CloudsData;", "dt", "", "sys", "Lcom/example/weatherandroidassignment/data/network/response/SunsetData;", "(ILjava/lang/String;IILcom/example/weatherandroidassignment/data/network/response/CoordinateData;Ljava/util/ArrayList;Ljava/lang/String;Lcom/example/weatherandroidassignment/data/network/response/MainData;Lcom/example/weatherandroidassignment/data/network/response/WindData;Lcom/example/weatherandroidassignment/data/network/response/CloudsData;JLcom/example/weatherandroidassignment/data/network/response/SunsetData;)V", "getBase", "()Ljava/lang/String;", "getClouds", "()Lcom/example/weatherandroidassignment/data/network/response/CloudsData;", "getCod", "()I", "getCoord", "()Lcom/example/weatherandroidassignment/data/network/response/CoordinateData;", "getDt", "()J", "getId", "getMain", "()Lcom/example/weatherandroidassignment/data/network/response/MainData;", "getName", "getSys", "()Lcom/example/weatherandroidassignment/data/network/response/SunsetData;", "getTimezone", "getWeather", "()Ljava/util/ArrayList;", "getWind", "()Lcom/example/weatherandroidassignment/data/network/response/WindData;", "component1", "component10", "component11", "component12", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "toString", "app_debug"})
public final class WeatherResponse {
    @androidx.room.PrimaryKey(autoGenerate = false)
    private final int id = 0;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String name = null;
    private final int cod = 0;
    private final int timezone = 0;
    @org.jetbrains.annotations.NotNull()
    private final com.example.weatherandroidassignment.data.network.response.CoordinateData coord = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.ArrayList<com.example.weatherandroidassignment.data.network.response.WeatherData> weather = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String base = null;
    @org.jetbrains.annotations.NotNull()
    private final com.example.weatherandroidassignment.data.network.response.MainData main = null;
    @org.jetbrains.annotations.NotNull()
    private final com.example.weatherandroidassignment.data.network.response.WindData wind = null;
    @org.jetbrains.annotations.NotNull()
    private final com.example.weatherandroidassignment.data.network.response.CloudsData clouds = null;
    private final long dt = 0L;
    @org.jetbrains.annotations.NotNull()
    private final com.example.weatherandroidassignment.data.network.response.SunsetData sys = null;
    
    public final int getId() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getName() {
        return null;
    }
    
    public final int getCod() {
        return 0;
    }
    
    public final int getTimezone() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.weatherandroidassignment.data.network.response.CoordinateData getCoord() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.example.weatherandroidassignment.data.network.response.WeatherData> getWeather() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getBase() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.weatherandroidassignment.data.network.response.MainData getMain() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.weatherandroidassignment.data.network.response.WindData getWind() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.weatherandroidassignment.data.network.response.CloudsData getClouds() {
        return null;
    }
    
    public final long getDt() {
        return 0L;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.weatherandroidassignment.data.network.response.SunsetData getSys() {
        return null;
    }
    
    public WeatherResponse(int id, @org.jetbrains.annotations.NotNull()
    java.lang.String name, int cod, int timezone, @org.jetbrains.annotations.NotNull()
    com.example.weatherandroidassignment.data.network.response.CoordinateData coord, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.example.weatherandroidassignment.data.network.response.WeatherData> weather, @org.jetbrains.annotations.NotNull()
    java.lang.String base, @org.jetbrains.annotations.NotNull()
    com.example.weatherandroidassignment.data.network.response.MainData main, @org.jetbrains.annotations.NotNull()
    com.example.weatherandroidassignment.data.network.response.WindData wind, @org.jetbrains.annotations.NotNull()
    com.example.weatherandroidassignment.data.network.response.CloudsData clouds, long dt, @org.jetbrains.annotations.NotNull()
    com.example.weatherandroidassignment.data.network.response.SunsetData sys) {
        super();
    }
    
    public final int component1() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    public final int component3() {
        return 0;
    }
    
    public final int component4() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.weatherandroidassignment.data.network.response.CoordinateData component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.example.weatherandroidassignment.data.network.response.WeatherData> component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.weatherandroidassignment.data.network.response.MainData component8() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.weatherandroidassignment.data.network.response.WindData component9() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.weatherandroidassignment.data.network.response.CloudsData component10() {
        return null;
    }
    
    public final long component11() {
        return 0L;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.weatherandroidassignment.data.network.response.SunsetData component12() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.weatherandroidassignment.data.network.response.WeatherResponse copy(int id, @org.jetbrains.annotations.NotNull()
    java.lang.String name, int cod, int timezone, @org.jetbrains.annotations.NotNull()
    com.example.weatherandroidassignment.data.network.response.CoordinateData coord, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.example.weatherandroidassignment.data.network.response.WeatherData> weather, @org.jetbrains.annotations.NotNull()
    java.lang.String base, @org.jetbrains.annotations.NotNull()
    com.example.weatherandroidassignment.data.network.response.MainData main, @org.jetbrains.annotations.NotNull()
    com.example.weatherandroidassignment.data.network.response.WindData wind, @org.jetbrains.annotations.NotNull()
    com.example.weatherandroidassignment.data.network.response.CloudsData clouds, long dt, @org.jetbrains.annotations.NotNull()
    com.example.weatherandroidassignment.data.network.response.SunsetData sys) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}