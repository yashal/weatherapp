package com.example.weatherandroidassignment.data.prefrences;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u000b\u001a\u0004\u0018\u00010\fJ\b\u0010\r\u001a\u0004\u0018\u00010\fJ\u000e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\fJ\u000e\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\fR\u0016\u0010\u0005\u001a\n \u0006*\u0004\u0018\u00010\u00030\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\b8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0013"}, d2 = {"Lcom/example/weatherandroidassignment/data/prefrences/PreferenceProvider;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "appContext", "kotlin.jvm.PlatformType", "preference", "Landroid/content/SharedPreferences;", "getPreference", "()Landroid/content/SharedPreferences;", "getCityName", "", "getLastSavedAt", "saveCityName", "", "name", "saveLastSavedAt", "savedAt", "app_debug"})
public final class PreferenceProvider {
    private final android.content.Context appContext = null;
    
    private final android.content.SharedPreferences getPreference() {
        return null;
    }
    
    public final void saveCityName(@org.jetbrains.annotations.NotNull()
    java.lang.String name) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCityName() {
        return null;
    }
    
    public final void saveLastSavedAt(@org.jetbrains.annotations.NotNull()
    java.lang.String savedAt) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLastSavedAt() {
        return null;
    }
    
    public PreferenceProvider(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
}