package com.example.weatherandroidassignment.data.db;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\bg\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\'J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0003H\'\u00a8\u0006\u0007"}, d2 = {"Lcom/example/weatherandroidassignment/data/db/MovieRepoDao;", "", "getTrendingRepo", "Lcom/example/weatherandroidassignment/data/network/response/WeatherResponse;", "saveAllTrendingRepo", "", "quote", "app_debug"})
public abstract interface MovieRepoDao {
    
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract void saveAllTrendingRepo(@org.jetbrains.annotations.NotNull()
    com.example.weatherandroidassignment.data.network.response.WeatherResponse quote);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM WeatherResponse")
    public abstract com.example.weatherandroidassignment.data.network.response.WeatherResponse getTrendingRepo();
}