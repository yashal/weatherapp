package com.example.weatherandroidassignment.data.prefrences;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0003"}, d2 = {"CITY", "", "KEY_SAVED_AT", "app_debug"})
public final class PreferenceProviderKt {
    private static final java.lang.String KEY_SAVED_AT = "key_saved_at";
    private static final java.lang.String CITY = "city";
}