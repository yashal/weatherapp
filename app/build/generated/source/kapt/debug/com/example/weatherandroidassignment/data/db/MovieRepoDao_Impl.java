package com.example.weatherandroidassignment.data.db;

import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.example.weatherandroidassignment.data.network.response.CloudsData;
import com.example.weatherandroidassignment.data.network.response.CoordinateData;
import com.example.weatherandroidassignment.data.network.response.MainData;
import com.example.weatherandroidassignment.data.network.response.SunsetData;
import com.example.weatherandroidassignment.data.network.response.WeatherData;
import com.example.weatherandroidassignment.data.network.response.WeatherResponse;
import com.example.weatherandroidassignment.data.network.response.WindData;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;

@SuppressWarnings({"unchecked", "deprecation"})
public final class MovieRepoDao_Impl implements MovieRepoDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<WeatherResponse> __insertionAdapterOfWeatherResponse;

  private final Converters __converters = new Converters();

  public MovieRepoDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfWeatherResponse = new EntityInsertionAdapter<WeatherResponse>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `WeatherResponse` (`id`,`name`,`cod`,`timezone`,`coord`,`weather`,`base`,`main`,`wind`,`clouds`,`dt`,`sys`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, WeatherResponse value) {
        stmt.bindLong(1, value.getId());
        if (value.getName() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getName());
        }
        stmt.bindLong(3, value.getCod());
        stmt.bindLong(4, value.getTimezone());
        final String _tmp;
        _tmp = __converters.coordinateDataToString(value.getCoord());
        if (_tmp == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, _tmp);
        }
        final String _tmp_1;
        _tmp_1 = __converters.weatherDataListToString(value.getWeather());
        if (_tmp_1 == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, _tmp_1);
        }
        if (value.getBase() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getBase());
        }
        final String _tmp_2;
        _tmp_2 = __converters.mainDataToString(value.getMain());
        if (_tmp_2 == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, _tmp_2);
        }
        final String _tmp_3;
        _tmp_3 = __converters.windDataToString(value.getWind());
        if (_tmp_3 == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, _tmp_3);
        }
        final String _tmp_4;
        _tmp_4 = __converters.cloudsDataToString(value.getClouds());
        if (_tmp_4 == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, _tmp_4);
        }
        stmt.bindLong(11, value.getDt());
        final String _tmp_5;
        _tmp_5 = __converters.cloudsDataToString(value.getSys());
        if (_tmp_5 == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindString(12, _tmp_5);
        }
      }
    };
  }

  @Override
  public void saveAllTrendingRepo(final WeatherResponse quote) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfWeatherResponse.insert(quote);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public WeatherResponse getTrendingRepo() {
    final String _sql = "SELECT * FROM WeatherResponse";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "name");
      final int _cursorIndexOfCod = CursorUtil.getColumnIndexOrThrow(_cursor, "cod");
      final int _cursorIndexOfTimezone = CursorUtil.getColumnIndexOrThrow(_cursor, "timezone");
      final int _cursorIndexOfCoord = CursorUtil.getColumnIndexOrThrow(_cursor, "coord");
      final int _cursorIndexOfWeather = CursorUtil.getColumnIndexOrThrow(_cursor, "weather");
      final int _cursorIndexOfBase = CursorUtil.getColumnIndexOrThrow(_cursor, "base");
      final int _cursorIndexOfMain = CursorUtil.getColumnIndexOrThrow(_cursor, "main");
      final int _cursorIndexOfWind = CursorUtil.getColumnIndexOrThrow(_cursor, "wind");
      final int _cursorIndexOfClouds = CursorUtil.getColumnIndexOrThrow(_cursor, "clouds");
      final int _cursorIndexOfDt = CursorUtil.getColumnIndexOrThrow(_cursor, "dt");
      final int _cursorIndexOfSys = CursorUtil.getColumnIndexOrThrow(_cursor, "sys");
      final WeatherResponse _result;
      if(_cursor.moveToFirst()) {
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        final String _tmpName;
        _tmpName = _cursor.getString(_cursorIndexOfName);
        final int _tmpCod;
        _tmpCod = _cursor.getInt(_cursorIndexOfCod);
        final int _tmpTimezone;
        _tmpTimezone = _cursor.getInt(_cursorIndexOfTimezone);
        final CoordinateData _tmpCoord;
        final String _tmp;
        _tmp = _cursor.getString(_cursorIndexOfCoord);
        _tmpCoord = __converters.stringToCoordinateData(_tmp);
        final ArrayList<WeatherData> _tmpWeather;
        final String _tmp_1;
        _tmp_1 = _cursor.getString(_cursorIndexOfWeather);
        _tmpWeather = __converters.stringToWeatherDataList(_tmp_1);
        final String _tmpBase;
        _tmpBase = _cursor.getString(_cursorIndexOfBase);
        final MainData _tmpMain;
        final String _tmp_2;
        _tmp_2 = _cursor.getString(_cursorIndexOfMain);
        _tmpMain = __converters.stringToMainData(_tmp_2);
        final WindData _tmpWind;
        final String _tmp_3;
        _tmp_3 = _cursor.getString(_cursorIndexOfWind);
        _tmpWind = __converters.stringToWindData(_tmp_3);
        final CloudsData _tmpClouds;
        final String _tmp_4;
        _tmp_4 = _cursor.getString(_cursorIndexOfClouds);
        _tmpClouds = __converters.stringToCloudsData(_tmp_4);
        final long _tmpDt;
        _tmpDt = _cursor.getLong(_cursorIndexOfDt);
        final SunsetData _tmpSys;
        final String _tmp_5;
        _tmp_5 = _cursor.getString(_cursorIndexOfSys);
        _tmpSys = __converters.stringToSunsetData(_tmp_5);
        _result = new WeatherResponse(_tmpId,_tmpName,_tmpCod,_tmpTimezone,_tmpCoord,_tmpWeather,_tmpBase,_tmpMain,_tmpWind,_tmpClouds,_tmpDt,_tmpSys);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
