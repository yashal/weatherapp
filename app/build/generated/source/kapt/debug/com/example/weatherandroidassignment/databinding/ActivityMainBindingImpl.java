package com.example.weatherandroidassignment.databinding;
import com.example.weatherandroidassignment.R;
import com.example.weatherandroidassignment.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityMainBindingImpl extends ActivityMainBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.mainContainer, 3);
        sViewsWithIds.put(R.id.addressContainer, 4);
        sViewsWithIds.put(R.id.updated_at, 5);
        sViewsWithIds.put(R.id.overviewContainer, 6);
        sViewsWithIds.put(R.id.status, 7);
        sViewsWithIds.put(R.id.temp, 8);
        sViewsWithIds.put(R.id.temp_min, 9);
        sViewsWithIds.put(R.id.temp_max, 10);
        sViewsWithIds.put(R.id.detailsContainer, 11);
        sViewsWithIds.put(R.id.sunrise, 12);
        sViewsWithIds.put(R.id.sunset, 13);
        sViewsWithIds.put(R.id.wind, 14);
        sViewsWithIds.put(R.id.pressure, 15);
        sViewsWithIds.put(R.id.humidity, 16);
        sViewsWithIds.put(R.id.about, 17);
        sViewsWithIds.put(R.id.loader, 18);
        sViewsWithIds.put(R.id.errorText, 19);
    }
    // views
    @NonNull
    private final androidx.coordinatorlayout.widget.CoordinatorLayout mboundView0;
    @NonNull
    private final android.widget.LinearLayout mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityMainBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 20, sIncludes, sViewsWithIds));
    }
    private ActivityMainBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (android.widget.TextView) bindings[17]
            , (android.widget.LinearLayout) bindings[4]
            , (android.widget.LinearLayout) bindings[11]
            , (android.widget.TextView) bindings[19]
            , (android.widget.TextView) bindings[16]
            , (android.widget.ProgressBar) bindings[18]
            , (android.widget.RelativeLayout) bindings[3]
            , (android.widget.LinearLayout) bindings[6]
            , (android.widget.TextView) bindings[15]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[2]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[14]
            );
        this.mboundView0 = (androidx.coordinatorlayout.widget.CoordinatorLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.search.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.errorModel == variableId) {
            setErrorModel((com.example.weatherandroidassignment.ui.viewmodel.ErrorModel) variable);
        }
        else if (BR.cityName == variableId) {
            setCityName((java.lang.String) variable);
        }
        else if (BR.viewModel == variableId) {
            setViewModel((com.example.weatherandroidassignment.ui.viewmodel.MainViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setErrorModel(@Nullable com.example.weatherandroidassignment.ui.viewmodel.ErrorModel ErrorModel) {
        this.mErrorModel = ErrorModel;
    }
    public void setCityName(@Nullable java.lang.String CityName) {
        this.mCityName = CityName;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.cityName);
        super.requestRebind();
    }
    public void setViewModel(@Nullable com.example.weatherandroidassignment.ui.viewmodel.MainViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelOnError((androidx.databinding.ObservableBoolean) object, fieldId);
            case 1 :
                return onChangeErrorModel((com.example.weatherandroidassignment.ui.viewmodel.ErrorModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelOnError(androidx.databinding.ObservableBoolean ViewModelOnError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeErrorModel(com.example.weatherandroidassignment.ui.viewmodel.ErrorModel ErrorModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int viewModelOnErrorViewGONEViewVISIBLE = 0;
        androidx.databinding.ObservableBoolean viewModelOnError = null;
        boolean viewModelOnErrorGet = false;
        java.lang.String cityName = mCityName;
        com.example.weatherandroidassignment.ui.viewmodel.MainViewModel viewModel = mViewModel;

        if ((dirtyFlags & 0x14L) != 0) {
        }
        if ((dirtyFlags & 0x19L) != 0) {



                if (viewModel != null) {
                    // read viewModel.onError
                    viewModelOnError = viewModel.getOnError();
                }
                updateRegistration(0, viewModelOnError);


                if (viewModelOnError != null) {
                    // read viewModel.onError.get()
                    viewModelOnErrorGet = viewModelOnError.get();
                }
            if((dirtyFlags & 0x19L) != 0) {
                if(viewModelOnErrorGet) {
                        dirtyFlags |= 0x40L;
                }
                else {
                        dirtyFlags |= 0x20L;
                }
            }


                // read viewModel.onError.get() ? View.GONE : View.VISIBLE
                viewModelOnErrorViewGONEViewVISIBLE = ((viewModelOnErrorGet) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
        }
        // batch finished
        if ((dirtyFlags & 0x14L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.search, cityName);
        }
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            this.search.setVisibility(viewModelOnErrorViewGONEViewVISIBLE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.onError
        flag 1 (0x2L): errorModel
        flag 2 (0x3L): cityName
        flag 3 (0x4L): viewModel
        flag 4 (0x5L): null
        flag 5 (0x6L): viewModel.onError.get() ? View.GONE : View.VISIBLE
        flag 6 (0x7L): viewModel.onError.get() ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}