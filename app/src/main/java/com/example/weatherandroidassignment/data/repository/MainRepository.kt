package com.example.weatherandroidassignment.data.repository

import androidx.lifecycle.MutableLiveData
import com.example.weatherandroidassignment.data.db.AppDataBase
import com.example.weatherandroidassignment.data.network.MyApi
import com.example.weatherandroidassignment.data.network.SafeApiRequest
import com.example.weatherandroidassignment.data.network.response.WeatherResponse
import com.example.weatherandroidassignment.data.prefrences.PreferenceProvider
import com.example.weatherandroidassignment.utils.Coroutines
import com.example.weatherandroidassignment.utils.getCurrentTime
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MainRepository(
    private val api: MyApi,
    private val db: AppDataBase,
    private val prefs: PreferenceProvider
) : SafeApiRequest() {

    private val quotes = MutableLiveData<WeatherResponse>()

    suspend fun getWeatherResult(cityName: String): WeatherResponse {
        val response = apiRequest { api.getWeatherDetails(cityName, METRIC, API_KEY) }
        quotes.postValue(response)
        return response
    }

    init {
        quotes.observeForever {
            saveReposInDataBase(it)
        }
    }

    private fun saveReposInDataBase(quotes: WeatherResponse){
        Coroutines.io{
            prefs.saveLastSavedAt(getCurrentTime().toString())
            db.getMovieRepoDao().saveAllTrendingRepo(quotes)
        }
    }

    suspend fun getReposFromDataBase(): WeatherResponse {
        return withContext(Dispatchers.IO){
            db.getMovieRepoDao().getTrendingRepo()
        }
    }

    companion object {
        const val API_KEY = "5ad7218f2e11df834b0eaf3a33a39d2a"
        const val METRIC = "metric"
    }
}