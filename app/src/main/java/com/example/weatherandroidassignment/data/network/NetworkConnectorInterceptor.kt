package com.example.weatherandroidassignment.data.network

import android.content.Context
import android.net.ConnectivityManager
import com.example.weatherandroidassignment.utils.NoInternetException
import com.example.weatherandroidassignment.utils.WifiNotConnectedException
import okhttp3.Interceptor
import okhttp3.Response


class NetworkConnectionInterceptor(
    context: Context
) : Interceptor {

    private val applicationContext = context.applicationContext

    override fun intercept(chain: Interceptor.Chain): Response {
        if (!isInternetAvailable())
            throw NoInternetException("Make sure you have active data connection")
        if (!isWifiConnected())
            throw WifiNotConnectedException("Make sure you have connected with wifi")
        return chain.proceed(chain.request())
    }

    private fun isInternetAvailable(): Boolean {

        // As depricated

        val connectivityManager =
            applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        connectivityManager.activeNetworkInfo.also {
            return it != null && it.isConnected
        }
    }

    private fun isWifiConnected(): Boolean {
        val connectivityManager =
            applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        connectivityManager.activeNetworkInfo.also {
            return it != null && it.type == ConnectivityManager.TYPE_WIFI
        }
    }
}