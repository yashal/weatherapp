package com.example.weatherandroidassignment.data.db

import androidx.room.TypeConverter
import com.example.weatherandroidassignment.data.network.response.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import kotlin.collections.ArrayList


class Converters {

    @TypeConverter
    fun stringToWeatherDataList(data: String?):
            ArrayList<WeatherData?> {

        val listType: Type =
            object : TypeToken<ArrayList<WeatherData?>?>() {}.type
        return Gson().fromJson(data, listType)
    }

    @TypeConverter
    fun weatherDataListToString(someObjects: ArrayList<WeatherData?>?): String? {
        return Gson().toJson(someObjects)
    }

    @TypeConverter
    fun stringToCoordinateData(data: String?):
            CoordinateData? {

        val objType: Type =
            object : TypeToken<CoordinateData?>() {}.type
        return Gson().fromJson(data, objType)
    }

    @TypeConverter
    fun coordinateDataToString(coordinateData: CoordinateData?): String? {
        return Gson().toJson(coordinateData)
    }

    @TypeConverter
    fun stringToMainData(data: String?):
            MainData? {

        val objType: Type =
            object : TypeToken<MainData?>() {}.type
        return Gson().fromJson(data, objType)
    }

    @TypeConverter
    fun mainDataToString(mainData: MainData?): String? {
        return Gson().toJson(mainData)
    }

    @TypeConverter
    fun stringToWindData(data: String?):
            WindData? {

        val objType: Type =
            object : TypeToken<WindData?>() {}.type
        return Gson().fromJson(data, objType)
    }

    @TypeConverter
    fun windDataToString(windData: WindData?): String? {
        return Gson().toJson(windData)
    }

    @TypeConverter
    fun stringToCloudsData(data: String?):
            CloudsData? {

        val objType: Type =
            object : TypeToken<CloudsData?>() {}.type
        return Gson().fromJson(data, objType)
    }

    @TypeConverter
    fun cloudsDataToString(cloudsData: CloudsData?): String? {
        return Gson().toJson(cloudsData)
    }

    @TypeConverter
    fun stringToSunsetData(data: String?):
            SunsetData? {

        val objType: Type =
            object : TypeToken<SunsetData?>() {}.type
        return Gson().fromJson(data, objType)
    }

    @TypeConverter
    fun cloudsDataToString(sunsetData: SunsetData?): String? {
        return Gson().toJson(sunsetData)
    }
}