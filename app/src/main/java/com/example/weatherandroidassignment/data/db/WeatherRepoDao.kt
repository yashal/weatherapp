package com.example.weatherandroidassignment.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.weatherandroidassignment.data.network.response.WeatherResponse

@Dao
interface WeatherRepoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAllTrendingRepo(quote: WeatherResponse)

    @Query("SELECT * FROM WeatherResponse")
    fun getTrendingRepo(): WeatherResponse
}