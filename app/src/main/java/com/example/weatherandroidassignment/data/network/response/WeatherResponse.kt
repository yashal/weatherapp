package com.example.weatherandroidassignment.data.network.response

import androidx.room.*

@Entity
data class WeatherResponse(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val name: String,
    val cod: Int,
    val timezone: Int,
    val coord: CoordinateData,
    val weather: ArrayList<WeatherData>,
    val base: String,
    val main: MainData,
    val wind: WindData,
    val clouds: CloudsData,
    val dt: Long,
    val sys: SunsetData
)

data class CoordinateData(
    val lon: Double,
    val lat: Double
)

data class WeatherData(
    val id: Int,
    val main: String,
    val description: String,
    val icon: String
)

data class MainData(
    val temp: Double,
    val feels_like: Double,
    val temp_max: Double,
    val temp_min: Double,
    val pressure: Int,
    val humidity: Int,
    val sea_level: Int,
    val grnd_level: Int
)

data class WindData(
    val speed: Double,
    val deg: Int
)

data class CloudsData(
    val all: Int
)

data class SunsetData(
    val country: String,
    val sunrise: Long,
    val sunset: Long
)
