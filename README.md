# README #

Weather App

### QUICK SUMMARY ###

* This Application is made for assignment purpose only

### Version ###
* VersionCode: 1


* VersionName: 1.0

### How do I get set up? ###

* Summary of set up: First I create a new project and set default language is Kotlin,
and give the package name and set up the project.


* Configuration: 


compileSdkVersion: 29


buildToolsVersion: "29.0.2"


minSdkVersion 21


targetSdkVersion 29


* Dependencies: Dependencies used for this project:


01. Design Dependencies


02. Jetbrains SDK


03. AppCompact


04. Core ktx


05. Basic Junit Test Cases and Mockito


06. GMS Play services


07. Life cycle and legacy support


08. Retrofit and GSON


09. Kotlin Coroutines


10. ViewModel and LiveData


11. New Material Design


12. Kodein Dependency Injection


13. Okhttp Log Interceptor


14. Card View


15. Lottie Animation


16. Android Room


17. Facebook shimmer effect


* Database configuration:


Room Database 


* How to run tests


Within src folder there are sub folder test. Expend it and right Click on package com.example.weatherandroidassignment and
click on run 'Test in weatherandroidassignment'


### Who do I talk to? ###

* Repo owner or admin: Yash BHARDWAJ


### Flow and Thought Process behind the logic ###
Step 01: First open the app launcher (either run by Android studio or click on launcher icon)


Step 02: If the os is grater or equals Marshmallow OS then it will ask for permission for location access. or it is less than Marshmallow then proceed with step 03


Step 03: It will ask to open your GPS access. once user give the GPS access it will get your latitude and longitude and get the city name. This whole process will be runs on Splash page.


Step 04: If it found the current city name then it navigates to Next page. Based on the city name it calls the api and get the result.


The Page contains the following info:


current City Name, current date, Sky info, Temperature, Minimum Temperature, Maximum Temperature,
Sunrise Time, Sunset Time, Wind Speed, Pressure, Humadity, Developed By(This result is static)


*Restrictions: Make sure user follows the below guideline otherwise not able to see the result.


If user does not provide the Permission, he will be out of the app. 


If user does not provide the location access, he will be out of the app.


If Internet is not open, user will see the error message with retry button.


If Internet is not connect with wi-fi, user will see the error message with retry button.


If API is sending any exception, user will see the error message with retry button.

If every thing is Ok then User can see the result from api. Once we get the result form API the we save the result  in local db.
Next time if user open the app then api is not called, data will be shown from the local db. 
This data will be shown from api for 2 hours from api call. After 2 hours again api will be called and then save the data and so on..

* If Location is changed then we will show the new data (api calling), this time no time restriction(api is called before 2 hours or not) 
